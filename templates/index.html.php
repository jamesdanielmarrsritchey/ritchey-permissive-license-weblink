<?php
#Template for /www/public/index.html
#Task
##Data
$location = realpath(dirname(__FILE__, 2));
eval(@substr(@file_get_contents("{$location}/evals/global_variables.php"), 5, -2));
$page = 'Ritchey Permissive License';
$content = <<<'NOWDOC'
<div style='border-color:#EEEEEE;border-width:3px;border-style:solid;padding:15px;margin-bottom:15px'>
Ritchey Permissive License v14:<br>
<br>
Subject to the terms of this license, any legal entity who receives material licensed under this license is granted royalty-free, perpetual, non-exclusive permission to do anything with the material which, does not violate any part of this license. Examples include copying, modifying, and sharing the material (verbatim, or modified), provided these actions can be completed in compliance with the license terms.<br>
<br>
If any concepts are presented in the material, permissions granted by this license cover the expression of such concepts, but do not extend to the concepts themselves (especially those protected by patents).<br>
<br>
Permissions granted by this license do not extend to trademarks included in the material, with the following exception: if sharing the material verbatim can be done in compliance with the terms of the license, it may be done even when trademarks are present.<br>
<br>
This license is automatically revoked permanently from the legal entity upon breach of the license terms.<br>
<br>
The material is provided "as is", without any warranties, guarantees, or representations. No promises are made of fitness for any particular purpose, suitability for merchantability, non infringement, accuracy, or absence of mistakes. Any use of the material is done at the legal entity's own risk.<br>
<br>
In the event of legal action in relation to the material, the maximum compensation to any party is nothing.<br>
<br>
When sharing the material, the legal entity is solely responsible for the consequences, and any withstanding obligations to the recipient (including: non disclaimed warranties, non disclaimed guarantees, any representations made).<br>
<br>
The material must entirely remain solely under this license.<br>
<br>
This license is governed by the laws of the province of British Columbia (as they were on April 21, 2019), and the applicable laws of Canada (as they were on April 21, 2019). Any legal proceedings related to this license may only occur in the courts of British Columbia, or Federal Court of Canada.<br>
<br>
The legal entity must be capable of being bound to this entire license, and agrees to be.<br>
<br>
If any portion of this license is known by the legal entity to have been previously deemed unenforceable, by a court of law, in applicable jurisdictions, this license cannot be accepted. The legal entity must make an effort to determine this before accepting this license.<br>
<br>
If any portions of this license contradict, the later portion shall overrule the previous.<br>
<br>
The license text is provided under these terms.<br>
</div>
<div>
<p>Download Text: <a href="https://jamesdanielmarrsritchey.gitlab.io/ritchey-permissive-license-weblink/uploads/5597b7b233ae9021f4794fb4feff55e2.txt">https://jamesdanielmarrsritchey.gitlab.io/ritchey-permissive-license-weblink/uploads/5597b7b233ae9021f4794fb4feff55e2.txt</a></p>
<p>Project Website: <a href="https://gitlab.com/jamesdanielmarrsritchey/ritchey-permissive-license">https://gitlab.com/jamesdanielmarrsritchey/ritchey-permissive-license</a></p>
</div>
NOWDOC;
eval(@substr(@file_get_contents("{$location}/evals/html_layout.php"), 5, -2));
##Write data
@file_put_contents("{$location}{$public}/index.html", $data);
?>
